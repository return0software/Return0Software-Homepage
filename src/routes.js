import React from 'react';
import { Link } from 'react-router-dom';
import { Route } from 'react-router';
import App from './containers/App';

const routes = [
    {
        path: '/',
        component: App,
        navVisible: true,
        navText: 'Home',
        exact: true
    },
];

export const routeComponents = routes.map(({ path, component, exact }, key) => <Route exact={exact} path={path} component={component} key={key} />);
export const navComponents = routes.map(({ path, navVisible, navText }, key) => navVisible ? <li key={key}><Link to={path}>{navText}</Link></li> : null);
